import React, {Component} from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import axios from 'axios'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';

import PokemonCard from './components/PokemonCard';
import SearchBar from './components/SearchBar';
import Battle from './components/Battle';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

class App extends Component {
  constructor(props){
    super(props);
    
    this.state = {
      collection: [],
      searchValue: '',
      suggestedPokemon: [],
      battleContender: {},
      battleDefender: {},
      open: false,
      
    };
  
    this.chooseForBattle = this.chooseForBattle.bind(this);
    this.handleAddingPokemon = this.handleAddingPokemon.bind(this)
    this.handleRemovingPokemon = this.handleRemovingPokemon.bind(this)
    this.updatePokemonSearch = this.updatePokemonSearch.bind(this)
    this.handleEndBattle = this.handleEndBattle.bind(this)
  }
  
  componentWillMount(){
    axios.get('http://localhost:5000/collection').then((response) => {
      this.setState({
        collection: response.data.data
      })
    })
  }
  
  updatePokemonSearch(searchValue) {
    if(searchValue !== ''){
      this.setState({
        searchValue,
      });
      const url = `http://localhost:5000/pokedex/${searchValue}`;
      axios.get(url).then((results) => {
        return results.data;
      }).then((results) => {
        const
          suggestedPokemon = results.data.map((pokemon) => {
            return {id: pokemon.id, name: pokemon.name};
          });
        this.setState({suggestedPokemon});
      });
    } else {
      this.setState({
        searchValue: '',
        suggestedPokemon: []
      })
    }
  }
  
  handleAddingPokemon(id) {
    const self = this;
    axios.post('http://localhost:5000/collection', {
      pokemonId: id
    })
      .then((result) => {
        self.setState({
          collection: result.data.data,
          searchValue: '',
          suggestedPokemon: []
        })
      })
  }
  
  handleRemovingPokemon(id) {
    const self = this;
    
    axios.delete('http://localhost:5000/collection/'+id)
      .then(() => {
        self.setState({
          collection: this.state.collection.filter(pokemon => {
            if(pokemon.id !== id){
              return true
            }
          })
        })
      })
  }
  
  chooseForBattle(contender){
    const self = this;
    const url = `http://localhost:5000/pokedex/random`;
    return axios.get(url)
      .then((defender) => {
        console.log(defender.data.data);
        self.setState({
          battleContender: contender,
          battleDefender: defender.data.data,
          open: true
        })
      })
  }
  
  handleEndBattle = () => {
    this.setState({open: false});
  };
  
  render() {
    console.log(this.state.collection)
    const collection = this.state.collection.map((pokemon, i) => {
      return (
        <PokemonCard
          key={i}
          id={pokemon.id}
          name={pokemon.name}
          type={pokemon.type}
          hp={pokemon.hp}
          attack={pokemon.attack}
          defense={pokemon.defense}
          chooseForBattle={() => this.chooseForBattle(pokemon)}
          removeFromCollection={() => this.handleRemovingPokemon(pokemon.id)}
        />
      )
    });
    
    let battle;
    if ('id' in this.state.battleContender) {
      battle = (
        <Battle
          contender={this.state.battleContender}
          defender={this.state.battleDefender}
        />
      );
    }
  
    const actions = [
      <FlatButton
        key={1}
        label="End Battle"
        primary
        onClick={this.handleEndBattle}
      />,
    ];
    
    return (
      <MuiThemeProvider>
        <div className="App">
          <AppBar
            title='Pokemon Battle App'
          />
          <SearchBar
            handleAddingPokemon={this.handleAddingPokemon}
            pokemon={this.state.pokemon}
            updatePokemonSearch={this.updatePokemonSearch}
            searchValue={this.state.searchValue}
            suggestedPokemon={this.state.suggestedPokemon}
          />
          <h1 style={{marginLeft: '15px', fontFamily: 'Roboto'}}>My Collection</h1>
          <div style={{display: 'flex', flexWrap: 'wrap', justifyContent: 'space-around'}}>
            {collection}
          </div>
          <Dialog
            title={`${this.state.battleContender.name} vs ${this.state.battleDefender.name}`}
            actions={actions}
            modal={true}
            open={this.state.open}
          >
            {battle}
          </Dialog>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
