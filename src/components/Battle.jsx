import React from 'react';
import PokemonCard from './PokemonCard';

const Battle = (props) => {
  let contenderResult, defenderResult;
  
  if(props.contender.total > props.defender.total){
    contenderResult = <p>WINNER</p>;
    defenderResult = <p>LOSER</p>;
  } else {
    contenderResult = <p>LOSER</p>;
    defenderResult = <p>WINNER</p>;
  }
  
  return (
    <div style={{display: 'flex', justifyContent: 'space-around'}}>
      <div>
        {contenderResult}
        <PokemonCard
          id={props.contender.id}
          name={props.contender.name}
          type={props.contender.type}
          hp={props.contender.hp}
          attack={props.contender.attack}
          defense={props.contender.defense}
        />
      </div>
      <div>
        {defenderResult}
        <PokemonCard
          id={props.defender.id}
          name={props.defender.name}
          type={props.defender.type}
          hp={props.defender.hp}
          attack={props.defender.attack}
          defense={props.defender.defense}
        />
      </div>
    </div>
  );
};

export default Battle;